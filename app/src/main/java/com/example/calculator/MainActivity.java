package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.mariuszgromada.math.mxparser.*;

public class MainActivity extends AppCompatActivity {
    private EditText display;
    private TextView tv;
    TextView history;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        tv=findViewById(R.id.tv);
        history=findViewById(R.id.hist);
        //history.getText(index);
        display=findViewById(R.id.input);

        display.setShowSoftInputOnFocus(false);

        display.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getString(R.string.display).equals(display.getText().toString())){
                    display.setText("");
                }
            }
        });

    }

    private void updateText(String strToAdd){
        String oldStr=display.getText().toString();
        int cursorPos=display.getSelectionStart();
        String leftStr=oldStr.substring(0,cursorPos);
        Log.e("clear",cursorPos+"");
        String rightStr=oldStr.substring(cursorPos);


        if(getString(R.string.display).equals(display.getText().toString())){
            display.setText(strToAdd);



        }else{
            display.setText(String.format("%s%s%s",leftStr,strToAdd,rightStr));
            tv.setText(String.format("%s%s%s",leftStr,strToAdd,rightStr));

        }

        display.setSelection(cursorPos+1);


    }
    //clear screen
    void clearScreen(){
        display.setText("");
    }

    //zero btn
    public void zeroBTN(View view){
        updateText("0");

    }

    //one button
    public void oneBTN(View view){
        updateText("1");

    }

    //second btn
    public void twoBTN(View view){
        updateText("2");

    }

    //three btn
    public void threeBTN(View view){
        updateText("3");

    }
    //four btn
    public void fourBTN(View view){
        updateText("4");

    }
    //five btn
    public void fiveBTN(View view){
        updateText("5");

    }
    //six btn
    public void sixBTN(View view){
        updateText("6");

    }
    //seven btn
    public void sevenBTN(View view){
        updateText("7");

    }
    //eight btn
    public void eightBTN(View view){
        updateText("8");

    }
    //nine btn
    public void nineBTN(View view){
        updateText("9");
    }
    //add btn
    //while (list.contains("+")){
    //s.o.pln(list)
    //int i=list.indexOf("+");
    //double cal=Double.parseDouble(list.get(i-1).toString())+Double.parseDouble(list.get(i+1).toString());
    //list.remove(i-1);
    //list.remove(i-1);
    //list.remove(i-1);
    //list.add(i-1,""+cal);}
    public void addBTN(View view){
        updateText("+");

    }
    //sub btn
    public void subBTN(View view){
        updateText("-");

    }
    //mul btn
    public void mulBTN(View view){
        updateText("x");

    }
    //div btn
    public void divBTN(View view){
        updateText("÷");

    }
    //plus nd minus btn
    public void plusMinusBTN(View view){
        updateText("-");

    }
    //par btn
    public void parentheseBTN(View view){

        int cursorPos=display.getSelectionStart();
        int openPar=0;
        int closePar=0;
        int textLen=display.getText().length();

        for(int i=0;i<cursorPos;i++){
            if(display.getText().toString().substring(i,i+1).equals("(")){
                openPar +=1;
            }
            if(display.getText().toString().substring(i,i+1).equals(")")){
                closePar +=1;
            }
        }
        if(openPar == closePar || display.getText().toString().substring(textLen-1,textLen).equals("")){
            updateText("(");

        }
        else if(closePar < openPar && !display.getText().toString().substring(textLen-1,textLen).equals("")){
            updateText(")");
        }
        display.setSelection(cursorPos+1);

    }
    //pint btn
    public void pointBTN(View view){
        updateText(".");

    }
    //exp btn
    public void exponentBTN(View view){
        updateText("^");
    }
    //backspace btn
    public void backspaceBTN(View view){
        int cursorPos=display.getSelectionStart();
        int textLen=display.getText().length();

        if(cursorPos !=0 &&  textLen !=0){
            SpannableStringBuilder selection=(SpannableStringBuilder) display.getText();
            selection.replace(cursorPos - 1,cursorPos,"");
            display.setText(selection);
            display.setSelection(cursorPos - 1 );
        }
    }
    //equal btn mxparsers
    public void equalBTN(View view){
        String userExp=display.getText().toString();

        tv.setText(userExp);


        userExp=userExp.replaceAll("÷","/");
        userExp=userExp.replaceAll("x","*");

        Expression exp=new Expression(userExp);
        String result=String.valueOf(exp.calculate());

        display.setText("="+result);
        display.setSelection(result.length());

    }
    //clear btn
    public void ClearBTN(View view){

        clearScreen();
        tv.setText("");
    }
}
