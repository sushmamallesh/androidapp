package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class HistoryActivity extends AppCompatActivity {
    String calhistory;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ((TextView)findViewById(R.id.tv_history)).setText(calhistory);
    }
    public void clearHistory(View view)
    {
        calhistory="";
        ((TextView)findViewById(R.id.tv_history)).setText(calhistory);
    }

}